package cz.martinvedra.genericsLibrary;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Book book1 = new Book("Don Quixote", "Miguel de Cervantes");
        Book book2 = new Book("Moby Dick", "Herman Melville");
        Book book3 = new Book("The Odyssey", "Homer");
        Book[] books = {book1, book2, book3};
        Library<Book> bookLibrary = new Library<>(books);

        Arrays.stream(bookLibrary.getElements())
                .forEach(x -> {
                    System.out.println(x);
                });
    }
}
