package cz.martinvedra.designPatterns.creational.singleton.eager.class_based;

public class Main {
    public static void main(String[] args) {
        SimpleCounter simpleCounterA = SimpleCounter.getSimpleCounterInstance();
        SimpleCounter simpleCounterB = SimpleCounter.getSimpleCounterInstance();
        System.out.println(simpleCounterA == simpleCounterB);

        simpleCounterA.incrementCount();
        simpleCounterB.incrementCount();
        System.out.println(simpleCounterA.getCount());
    }
}
