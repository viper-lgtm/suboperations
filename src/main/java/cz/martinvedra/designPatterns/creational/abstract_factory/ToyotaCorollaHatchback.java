package cz.martinvedra.designPatterns.creational.abstract_factory;

public class ToyotaCorollaHatchback extends ToyotaCorolla {
    @Override
    public Type getType() {
        return Type.HATCHBACK;
    }

    @Override
    public Integer getCylindersNum() {
        return 4;
    }

    @Override
    public Float getEngineVolume() {
        return 2.0F;
    }

    @Override
    public Integer getTrunkSize() {
        return 420;
    }
}
