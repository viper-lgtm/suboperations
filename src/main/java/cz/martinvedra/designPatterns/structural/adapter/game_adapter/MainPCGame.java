package cz.martinvedra.designPatterns.structural.adapter.game_adapter;

public class MainPCGame {
    public static void main(String[] args) {
        PCGame pcGame = new ComputerGameAdapter(new ComputerGame(
                "CSGO",
                PegiAgeRating.P16,
                60.0,
                2,
                600,
                8,
                8,
                5.4
        ));
        System.out.println(pcGame);
    }
}
