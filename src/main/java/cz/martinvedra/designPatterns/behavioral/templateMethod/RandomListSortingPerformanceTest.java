package cz.martinvedra.designPatterns.behavioral.templateMethod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomListSortingPerformanceTest extends PerformanceTestTemplate {
    private static final int NUMBERS_NUM = 100000;
    @Override
    protected int getWarmUpIterationsNum() {
        return 2;
    }

    @Override
    protected int getIterationsNum() {
        return 100;
    }

    @Override
    protected void iteration() {
        List<Integer> integers = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < NUMBERS_NUM; i++) {
            integers.add(random.nextInt());
        }
        Collections.sort(integers);
    }
}
