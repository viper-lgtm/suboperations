package cz.martinvedra.designPatterns.behavioral.observer.subject;

public class SpecificValueObserver extends Observer {

    public SpecificValueObserver(Subject subject) {
        super(subject);
    }

    @Override
    public void update() {
        System.out.println("Specific value observer: " + subject.getState());
    }
}
