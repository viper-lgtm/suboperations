package cz.martinvedra.designPatterns.behavioral.strategy;

public enum StrategyType {
    DOUBLE,
    REMOVE,
    REPLACE
}
