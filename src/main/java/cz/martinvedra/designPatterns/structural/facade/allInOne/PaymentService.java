package cz.martinvedra.designPatterns.structural.facade.allInOne;

public interface PaymentService {
    void pay(Long productId, int amount);
}
