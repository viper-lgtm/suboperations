package cz.martinvedra.designPatterns.creational.builder.outsideClass;

public class ToyBuilder {

    private String name;
    private String color;

    public ToyBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public ToyBuilder withColor(String color) {
        this.color = color;
        return this;
    }

    public Toy build() {
        return new Toy(name, color);
    }
}
