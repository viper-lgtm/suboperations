package cz.martinvedra.celsiusToFahrenheit;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert temperature in Celsius");
        float celsius = scanner.nextFloat();
        System.out.println("Temperature in Fahrenheit degrees is " + celsius * 1.8f + 32);
    }
}
