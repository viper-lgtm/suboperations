package cz.martinvedra.designPatterns.structural.adapter.game_adapter;

public interface PCGame {
    String getTitle();
    Integer getPegiAllowedAge();
    boolean isTripleAGame();
    Requirements getRequirements();
}
