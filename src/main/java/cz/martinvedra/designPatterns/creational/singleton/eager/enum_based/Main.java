package cz.martinvedra.designPatterns.creational.singleton.eager.enum_based;

public class Main {
    public static void main(String[] args) {
        SimpleCounter simpleCounterA = SimpleCounter.SIMPLE_COUNTER_INSTANCE;
        SimpleCounter simpleCounterB = SimpleCounter.SIMPLE_COUNTER_INSTANCE;

        System.out.println(simpleCounterA == simpleCounterB);

        simpleCounterA.incrementCount();
        simpleCounterB.incrementCount();

        System.out.println(simpleCounterA.getCount());
    }
}
