package cz.martinvedra.designPatterns.creational.builder.insideClass;

import java.util.List;

public class WeaponMain {
    public static void main(String[] args) {
        final Weapon laserGun = new Weapon.Builder()
                .withDamage(123)
                .withName("LaserGun")
                .withPerks(List.of("Color:red"))
                .withDurability(50L)
                .build();
        System.out.println(laserGun);
    }
}
