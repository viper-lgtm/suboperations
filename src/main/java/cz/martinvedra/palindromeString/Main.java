package cz.martinvedra.palindromeString;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String s = scanner.nextLine();
            if (s.equals("exit")) {
                break;
            }

            palindromeRecursion(s);
//            System.out.println("Is a palindrome: " + isPalindrome(s));
//            palindromeTwoPoints(s);
//            palindromeCharAtHalf(s);
//            palindromeStringBuilderReverse(s);
//            palindromeStringBuilderAppend(s);
//            palindromeStringReverse(s);
        }
    }

    private static void palindromeRecursion(String s) {
        palindromeRecursion(s, 0, s.length() - 1);
    }

    private static void palindromeRecursion(String s, int i, int j) {
        s = s.replaceAll("\\s+", "").toLowerCase();
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                System.out.println("Is not a palindrome");
                return;
            }
            palindromeRecursion(s, i + 1, j - 1);
            return;
        }
        System.out.println("Is a palindrome");
    }



    private static boolean isPalindrome(String s, int i, int j) {
        if (i >= j) {
            return true;
        }

        if (s.charAt(i) != s.charAt(j)) {
            return false;
        }

        return isPalindrome(s, i + 1, j - 1);
    }

    private static boolean isPalindrome(String s) {
        return isPalindrome(s, 0, s.length() - 1);
    }

    private static void palindromeTwoPoints(String s) {
        s = s.replaceAll("\\s+", "").toLowerCase();
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                System.out.println("Is not a palindrome");
                return;
            }
            i++;
            j--;
        }
        System.out.println("Is a palindrome");
    }


    private static void palindromeCharAtHalf(String s) {
        s = s.replaceAll(" ", "").toLowerCase();
        for (int i = 0; i < s.length() / 2; i++) {
            if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
                System.out.println("Is not a palindrome");
                return;
            }
        }
        System.out.println("Is a palindrome");
    }

    private static void palindromeStringBuilderReverse(String s) {
        s = s.replaceAll("\\s+", "").toLowerCase();
        StringBuilder sb = new StringBuilder(s);

        sb = sb.reverse();

        if (s.equals(sb.toString())) {
            System.out.println("Is a palindrome");
        } else {
            System.out.println("Is not a palindrome");
        }
    }

    private static void palindromeStringBuilderAppend(String s) {
        s = s.replaceAll("\\s+", "").toLowerCase();
        StringBuilder sb = new StringBuilder();

        for (int i = s.length() - 1; i >= 0; i--) {
            sb.append(s.charAt(i));
        }

        if (s.equals(sb.toString())) {
            System.out.println("Is a palindrome");
        } else {
            System.out.println("Is not a palindrome");
        }
    }

    private static void palindromeStringReverse(String s) {
        s = s.replaceAll(" ", "").toLowerCase();
        String rev = "";

        for (int i = s.length() - 1; i >= 0; i--) {
            rev += s.charAt(i);

        }

        if (rev.equals(s)) {
            System.out.println("Is a palindrome");
        } else {
            System.out.println("Is not a palindrome");
        }
    }


}
