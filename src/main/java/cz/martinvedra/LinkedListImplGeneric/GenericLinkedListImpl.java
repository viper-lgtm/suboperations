package cz.martinvedra.LinkedListImplGeneric;

public class GenericLinkedListImpl<T> {

    private Node<T> head;
    private int length = 0;

    public GenericLinkedListImpl() {
        this.head = null;
    }

    public void add(T data) {
        Node<T> newNode = new Node<>(data);
        if (head == null) {
            head = newNode;
        } else {
            Node<T> temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = newNode;
        }
        length++;
    }

    public void addAtPosition(T data, int position) {
        if (position > length + 1) {
            System.out.println("Position Unavailable in LinkedList");
            return;
        }

        Node<T> newNode = new Node<>(data);
        if (position == 1) {
            Node<T> temp = head;
            head = newNode;
            head.next = temp;
            length++;
            return;
        }

        Node<T> temp = head;
        Node<T> prev = new Node<>(null);
        while (position - 1 > 0) {
            prev = temp;
            temp = temp.next;
            position--;
        }
        prev.next = newNode;
        prev.next.next = temp;
        length++;
    }

    public void removeByKey(T key) {
        boolean exists = false;

        if (head.data == key) {
            head = head.next;
            exists = true;
        }

        Node<T> prev = new Node<>(null);
        prev.next = head;
        Node<T> next = head.next;
        Node<T> temp = head;

        while (temp.next != null) {
            if (String.valueOf(temp.data).equals(String.valueOf(key))) {
                prev.next = next;
                exists = true;
                break;
            }
            prev = temp;
            temp = temp.next;
            next = temp.next;
        }

        if (exists == false && String.valueOf(temp.data).equals(String.valueOf(key))) {
            prev.next = null;
            exists = true;
        }

        if (exists) {
            length--;
        } else {
            System.out.println("Value not present");
        }
    }

    public void clear() {
        head = null;
        length = 0;
    }

    public boolean isEmpty() {
        if (head == null) {
            return true;
        }
        return false;
    }

    public int getListLength() {
        return length + 1;
    }

    @Override
    public String toString() {
        String s = "{ ";
        Node<T> temp = head;
        if (temp == null) {
            return s + " }";
        }

        while (temp.next != null) {
            s += String.valueOf(temp.data) + " -> ";
            temp = temp.next;
        }

        s += String.valueOf(temp.data);
        return s + " }";
    }
}
