package cz.martinvedra.designPatterns.behavioral.observer.subject;

public class SubjectMain {
    public static void main(String[] args) {
        Subject subject = new Subject();
        ByTenChangedObserver byTenChangedObserver = new ByTenChangedObserver(subject);
        subject.changeStateBy(20);
        byTenChangedObserver.update();
    }
}
