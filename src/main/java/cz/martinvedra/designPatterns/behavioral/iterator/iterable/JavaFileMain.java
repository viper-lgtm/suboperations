package cz.martinvedra.designPatterns.behavioral.iterator.iterable;

import java.util.Iterator;
import java.util.List;

public class JavaFileMain {
    public static void main(String[] args) {
        JavaFile javaFile = new JavaFile("Home.java", "Home", List.of("Hello", "World"));

        Iterator<String> iterator = javaFile.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}

