package cz.martinvedra.designPatterns.structural.facade.seperate;

public interface Encryptor {
    String encrypt(String toEncrypt);
}
