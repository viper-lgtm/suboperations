package cz.martinvedra.sumSequence;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert two integers, first smaller, and it will calculate the sum of numbers between them including");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int sum = 0;
        if (a < b) {
            for (int i = a; i <= b; i++) {
                sum += i;
            }
            System.out.println(sum);
        } else {
            System.out.println("Wrong input");
        }
    }
}
