package cz.martinvedra.designPatterns.creational.singleton.eager.enum_based;

public enum SimpleCounter {
    SIMPLE_COUNTER_INSTANCE;
    private int count = 0;

    public void incrementCount() {
        count++;
    }

    public int getCount() {
        return count;
    }
}
