package cz.martinvedra.designPatterns.structural.adapter.game_adapter;

public enum PegiAgeRating {
    P3, P7, P12, P16, P18
}
