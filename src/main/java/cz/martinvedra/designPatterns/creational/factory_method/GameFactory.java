package cz.martinvedra.designPatterns.creational.factory_method;

public interface GameFactory {
    Game create();
}
