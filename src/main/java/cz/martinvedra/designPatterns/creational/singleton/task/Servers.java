package cz.martinvedra.designPatterns.creational.singleton.task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Servers {
    private static Servers servers;
    private final List<String> serverList;
    private Servers() {
        serverList = new ArrayList<>();
    }

    public Servers getServersInstance() {
        if (servers == null) {
            servers = new Servers();
        }
        return servers;
    }

    public boolean addServer(String server) {
        if ((server.startsWith("http") || server.startsWith("https")) && !serverList.contains(server)) {
            return serverList.add(server);
        }
        return false;
    }

    public List<String> getHttpServers() {
        return serverList.stream()
                .filter(server -> server.startsWith("http"))
                .collect(Collectors.toUnmodifiableList());
    }

    public List<String> getHttpsServers() {
        return serverList.stream()
                .filter(server -> server.startsWith("https"))
                .collect(Collectors.toUnmodifiableList());
    }

    private List<String> getServersStartingWith(String prefix) {
        return serverList.stream()
                .filter(server -> server.startsWith(prefix))
                .collect(Collectors.toUnmodifiableList());
    }


}
