package cz.martinvedra.designPatterns.structural.facade.seperate;

public class NoOpEncryptor implements Encryptor {
    @Override
    public String encrypt(final String toEncrypt) {
        return toEncrypt;
    }
}
