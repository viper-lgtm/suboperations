package cz.martinvedra.occurencesNumbers;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Count number occurence in the sequence");
        int[] nums = {6, 5, 4, 5, 10, 5, 8, 3, 10, 6, 6, 6, 4, 3, 2, 8, 1, 3, 4, 7, 5};
        int[] occurences = new int[10];
        for (int i = 0; i < nums.length; i++) {
            occurences[nums[i] - 1] += 1;
        }

        for (int i = 0; i < occurences.length; i++) {
            System.out.println(i + 1 + " - " + occurences[i] + "x");
        }
    }
}
