package cz.martinvedra.powersOfTwo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert number and it will print all powers of 2 not greater then inserted number");
        int n = scanner.nextInt();
        int powerOfTwo = 1;
        for (int i = 1; i <= n; i++) {
            while (powerOfTwo < n) {
                System.out.println(powerOfTwo);
                powerOfTwo *= 2;
            }
        }
    }
}
