package cz.martinvedra.designPatterns.structural.decorator.fragger;

public class DeathCountInfoDecorator implements FragStatistics {
    private FragStatistics fragStatistics;

    public DeathCountInfoDecorator(FragStatistics fragStatistics) {
        this.fragStatistics = fragStatistics;
    }

    @Override
    public int incrementFragCount() {
        return fragStatistics.incrementFragCount();
    }

    @Override
    public int incrementDeathCount() {
        System.out.println("Fragged by an enemy");
        return fragStatistics.incrementDeathCount();
    }

    @Override
    public void reset() {
        fragStatistics.reset();
    }
}
