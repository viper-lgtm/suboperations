package cz.martinvedra.designPatterns.behavioral.interpreter;

public interface Interpreter {
    String interpret(String context);
}
