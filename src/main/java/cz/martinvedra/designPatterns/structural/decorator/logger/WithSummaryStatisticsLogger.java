package cz.martinvedra.designPatterns.structural.decorator.logger;

import java.util.List;

public class WithSummaryStatisticsLogger implements StatisticsLogger {
    private final StatisticsLogger statisticsLogger;

    public WithSummaryStatisticsLogger(StatisticsLogger statisticsLogger) {
        this.statisticsLogger = statisticsLogger;
    }

    @Override
    public void displayStatistics() {
        System.out.println("With summary statistics start");
        System.out.println(getExecutionTimes().stream().mapToDouble(x -> x).summaryStatistics());
        statisticsLogger.displayStatistics();
        System.out.println("With summary statistics end");
    }

    @Override
    public List<Double> getExecutionTimes() {
        return statisticsLogger.getExecutionTimes();
    }
}
