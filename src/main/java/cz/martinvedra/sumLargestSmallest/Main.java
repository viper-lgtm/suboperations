package cz.martinvedra.sumLargestSmallest;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Keep inserting numbers, number 0 is end and program calculates sum of max and min number and their average");
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int n = scanner.nextInt();

        while (n != 0) {
            if (n < min) {
                min = n;
            }

            if (n > max) {
                max = n;
            }
            n = scanner.nextInt();
        }

        System.out.println("Min " + min);
        System.out.println("Max " + max);
        System.out.println("Sum " + (min + max));
        System.out.println("Average = " + (min + max) / 2f);
    }
}
