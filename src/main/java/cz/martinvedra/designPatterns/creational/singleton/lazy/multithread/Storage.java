package cz.martinvedra.designPatterns.creational.singleton.lazy.multithread;

import java.util.ArrayList;
import java.util.List;

public class Storage {
    private static Storage storage;
    private List<Integer> integerList = new ArrayList<>();

    private Storage() {
    }

    public static Storage getStorageInstance() {
        if (storage == null) {
            synchronized (Storage.class) {
                if (storage == null) {
                    storage = new Storage();
                }
            }
        }
        return storage;
    }

    public void addToList(int num) {
        integerList.add(num);
    }

    public List<Integer> printList() {
        return integerList;
    }
}
