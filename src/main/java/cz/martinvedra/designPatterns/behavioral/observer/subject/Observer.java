package cz.martinvedra.designPatterns.behavioral.observer.subject;

public abstract class Observer {
    protected Subject subject;

    public Observer(Subject subject) {
        this.subject = subject;
        subject.subscribe(this);
    }

    public abstract void update();
}
