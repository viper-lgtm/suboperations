package cz.martinvedra.designPatterns.behavioral.chainOfResponsibility;

public interface Credentials {
    String getCredentials(String userId);
}
