package cz.martinvedra.designPatterns.structural.decorator.fragger;

import static java.util.Objects.nonNull;

public class FragDeathRatioDecorator implements FragStatistics {
    private FragStatistics fragStatistics;
    private Integer currentFragCount = null;
    private Integer currentDeathCount = null;

    public FragDeathRatioDecorator(FragStatistics fragStatistics) {
        this.fragStatistics = fragStatistics;
    }

    @Override
    public int incrementFragCount() {
        currentFragCount = fragStatistics.incrementFragCount();
        displayFragDeathsRatio();
        return currentFragCount;
    }

    @Override
    public int incrementDeathCount() {
        currentDeathCount = fragStatistics.incrementDeathCount();
        displayFragDeathsRatio();
        return currentDeathCount;
    }

    @Override
    public void reset() {
        fragStatistics.reset();
    }

    private void displayFragDeathsRatio() {
        if (nonNull(currentFragCount) && nonNull(currentDeathCount)) {
            System.out.println("KDR is " + (double) currentFragCount / currentDeathCount);
        }
    }
}
