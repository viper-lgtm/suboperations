package cz.martinvedra.designPatterns.creational.prototype;

public class JavaCodeFileManager {
    private static final JavaCodeFile BASE_FILE = new JavaCodeFile("SDA Licence", "java");

    public JavaCodeFile createFileWithContent(String fileName, String content) throws CloneNotSupportedException {
        JavaCodeFile baseFileClone = BASE_FILE.createClone();
        baseFileClone.setCode(content);
        baseFileClone.setFileName(fileName);
        return baseFileClone;
    }
}
