package cz.martinvedra.designPatterns.creational.prototype;

public class JavaCodeMain {
    public static void main(String[] args) throws CloneNotSupportedException {
        JavaCodeFileManager javaCodeFileManager = new JavaCodeFileManager();
        JavaCodeFile fileA = javaCodeFileManager.createFileWithContent("Integers", "int x = 3;");
        JavaCodeFile fileB = javaCodeFileManager.createFileWithContent("Strings", "String y = \"3\";");
        System.out.println(fileA);
        System.out.println(fileB);
    }
}
