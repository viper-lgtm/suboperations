package cz.martinvedra.morseMap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MapMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<Character, String> map = new HashMap<>();
        map.put('a', ".-");
        map.put('b', "-...");
        map.put('c', "-.-.");
        map.put('d', "-..");
        map.put('e', ".");
        map.put('f', "..-.");
        map.put('g', "--.");
        map.put('h', "....");
        map.put('i', "..");
        map.put('j', ".---");
        map.put('k', "-.-");
        map.put('l', ".-..");
        map.put('m', "--");
        map.put('n', "-.");
        map.put('o', "---");
        map.put('p', ".--.");
        map.put('q', "--.-");
        map.put('r', ".-.");
        map.put('s', "...");
        map.put('t', "-");
        map.put('u', "..-");
        map.put('v', "...-");
        map.put('w', ".--");
        map.put('x', "-..-");
        map.put('y', "-.--");
        map.put('z', "--..");
        map.put(' ', ".......");

        Map<String, Character> morseToCharMap = new HashMap<>();
        for (Map.Entry<Character, String> entry : map.entrySet()) {
            morseToCharMap.put(entry.getValue(), entry.getKey());
        }

        while (true) {
            System.out.println("MENU");
            System.out.println("Write command: \n'm' for Word -> Morse translator\n'w' for Morse -> Word translator, \n'*' for Exit the program");
            String cmd = scanner.nextLine();
            if ("*".equals(cmd)) {
                break;
            }
            switch (cmd) {
                case "m" -> {
                    System.out.println("Word -> Morse translator");
                    System.out.println("Write word and press Enter.\nFor Exit to Main Menu press '*'");
                    while (true) {
                        String word = scanner.nextLine().toLowerCase().replaceAll("\\s+", "");
                        if ("*".equals(word)) {
                            break;
                        }
                        for (int i = 0; i < word.length(); i++) {
                            System.out.print(map.get(word.charAt(i)) + "/");
                        }
                        System.out.println();
                    }
                }
                case "w" -> {
                    System.out.println("Morse -> Word translator");
                    System.out.println("Write morse code, group of characters separated by / and press Enter.\nFor Exit to Main Menu press '*'");
                    while (true) {
                        String[] strings = scanner.nextLine().split("/");
                        boolean x = Arrays.stream(strings).anyMatch(i -> i.equals("*"));
                        if (x) {
                            break;
                        }
                        for (String input : strings) {
                            System.out.print(morseToCharMap.get(input));
                        }
                    }
                }
            }
        }
    }
}
