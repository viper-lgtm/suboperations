package cz.martinvedra.designPatterns.behavioral.mediator;

public interface Mediator {
    void sendInfo(Object requester, String context);
}
