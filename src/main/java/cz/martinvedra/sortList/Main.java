package cz.martinvedra.sortList;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        System.out.println("Collections.sort - according to ASCI, is case insensitive ---> 'a' is after 'Z'");
        List<String> s1 = Arrays.asList("A", "B", "C", "Z", "a", "D");
        Collections.sort(s1);
        System.out.println(s1);
        System.out.println();

        System.out.println("Collections.reverseOrder - opposite to sort");
        List<String> s2 = Arrays.asList("A", "B", "C", "Z", "a", "D");
        Collections.sort(s2, Collections.reverseOrder());
        System.out.println(s2);
        System.out.println();

        System.out.println("Collections.sort lambda, alphabetically reversed, case sensitive");
        List<String> s5 = Arrays.asList("A", "B", "C", "Z", "a", "D");
        Collections.sort(s5, (l1, l2) -> l2.compareToIgnoreCase(l1));
        System.out.println(s5);
        System.out.println();


        System.out.println("Collections.sort alphabetically reversed, parameter new Comparator");
        List<String> s = Arrays.asList("A", "B", "C", "D", "a", "Z");
        Collections.sort(s, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareToIgnoreCase(o1);
            }
        });
        System.out.println(s);
        System.out.println();

        System.out.println("Sort stream");
        System.out.println("Like Collections.sort, according to ASCII, CASE INSENSITIVE");
        Stream<String> strStream = List.of("A", "Z", "E", "D", "a").stream();
        System.out.println(strStream
                .sorted()
                .toList());
        System.out.println();

        System.out.println("Sort stream reversed");
        Stream<String> strStream1 = List.of("A", "Z", "E", "D", "a").stream();
        System.out.println(strStream1
                .sorted(Comparator.reverseOrder())
                .toList());
        System.out.println();

        System.out.println("Sort stream lambda, alphabetically reversed");
        List<String> strings = List.of("A", "B", "C", "Z", "a", "D");
        System.out.println(strings.stream()
                .sorted((o1, o2) -> o2.compareToIgnoreCase(o1))
                .collect(Collectors.toUnmodifiableList()));
        System.out.println();

        System.out.println("Sort stream new Comparator, alphabetically reversed");
        List<String> strings2 = List.of("A", "B", "C", "Z", "a", "D");
        System.out.println(strings2.stream()
                .sorted(new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return o2.compareToIgnoreCase(o1);
                    }
                })
                .collect(Collectors.toUnmodifiableList()));
        System.out.println();


        System.out.println("Collections.reverse - just reversed input, no alphabetical order");
        List<String> s3 = Arrays.asList("A", "B", "C", "Z", "a", "D");
        Collections.reverse(s3);
        System.out.println(s3);
        System.out.println();

        System.out.println("Iterator");
        List<String> s4 = Arrays.asList("A", "B", "C", "Z", "a", "D");
        Iterator iter = s4.iterator();
        while (iter.hasNext()) {
            System.out.print(iter.next() + ", ");
        }
        System.out.println();
        System.out.println();
    }
}
