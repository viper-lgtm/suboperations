package cz.martinvedra.designPatterns.structural.adapter.pupil_adapter;

import java.util.Collection;

public interface Student {
    String getFullName();
    String getContactDetails();
    boolean isAdult();
    Collection<Integer> getResults();
}
