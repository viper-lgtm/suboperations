package cz.martinvedra.designPatterns.behavioral.templateMethod;

public class PerformanceMain {
    public static void main(String[] args) {
//        PerformanceTestTemplate testTemplate = new RandomListSortingPerformanceTest();
//        testTemplate.run();

        PerformanceTestTemplate testTemplate1 = new StringBuilderAppendPerformanceTest();
        testTemplate1.run();
    }
}
