package cz.martinvedra.recursion_fibo_factorial.fibonacci;

public class FiboRecursion1 {
    static int n1 = 0;
    static int n2 = 1;
    static int n3;
    public static void main(String[] args) {
        fibo(10);
    }

    private static void fibo(int end) {
        if (end > 0) {
            System.out.print(n1 + " ");
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            fibo(end - 1);
        }
    }
}
