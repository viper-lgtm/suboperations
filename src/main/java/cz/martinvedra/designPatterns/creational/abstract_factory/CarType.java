package cz.martinvedra.designPatterns.creational.abstract_factory;

public enum  CarType {
    TOYOTA_COROLLA,
    AUDI_A4
}
