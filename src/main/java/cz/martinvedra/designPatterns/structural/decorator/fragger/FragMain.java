package cz.martinvedra.designPatterns.structural.decorator.fragger;

public class FragMain {
    public static void main(String[] args) {
        FragStatistics statistics = new FirstPersonShooterFragStatistics();
        statistics.incrementFragCount();
        statistics.incrementDeathCount();

        FragStatistics decoratedStatistics = new FragDeathRatioDecorator(new FragInfoDecorator(new DisplayCountersDecorator(new DeathCountInfoDecorator(statistics))));

        decoratedStatistics.incrementFragCount();
        decoratedStatistics.incrementFragCount();
        decoratedStatistics.incrementFragCount();
        decoratedStatistics.incrementDeathCount();
    }
}
