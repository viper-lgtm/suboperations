package cz.martinvedra.designPatterns.structural.facade.allInOne;

public interface ProductAvailabilityService {
    boolean isAvailable(Long productId);
}
