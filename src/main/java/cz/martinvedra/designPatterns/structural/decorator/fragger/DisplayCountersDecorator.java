package cz.martinvedra.designPatterns.structural.decorator.fragger;

public class DisplayCountersDecorator implements FragStatistics {
    private FragStatistics fragStatistics;

    public DisplayCountersDecorator(FragStatistics fragStatistics) {
        this.fragStatistics = fragStatistics;
    }

    @Override
    public int incrementFragCount() {
        int fragCount = fragStatistics.incrementFragCount();
        System.out.println("Your frag is now " + fragCount);
        return fragCount;
    }

    @Override
    public int incrementDeathCount() {
        int deathCount = fragStatistics.incrementDeathCount();
        System.out.println("Your death count is now " + deathCount);
        return deathCount;
    }

    @Override
    public void reset() {
        fragStatistics.reset();
        System.out.println("Stats reset - KDR is equal to 0");
    }
}
