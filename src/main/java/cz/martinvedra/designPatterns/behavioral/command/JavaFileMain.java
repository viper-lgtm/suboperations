package cz.martinvedra.designPatterns.behavioral.command;

import java.util.List;

public class JavaFileMain {
    public static void main(String[] args) {
        JavaFile javaFile = new JavaFile("Commands.java", "Commands",
                List.of("Command {", " ", "private String content;", "}"));
        Command changeFileNameCommand = new ChangeFileNameCommand(javaFile, "UpdatedCommands.java");
        Command removeEmptyLinesCommand = new RemoveEmptyLinesCommand(javaFile);
        System.out.println(javaFile);
        removeEmptyLinesCommand.apply();
        System.out.println(javaFile);
        changeFileNameCommand.apply();
        System.out.println(javaFile);
//        System.out.println(javaFile);
//        changeFileNameCommand.cancel();

    }
}
