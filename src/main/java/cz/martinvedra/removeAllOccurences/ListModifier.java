package cz.martinvedra.removeAllOccurences;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ListModifier {

    public List<Integer> removeAllOccurrencesStreamFilterNewList(List<Integer> list, int element) {
        return list.stream()
                .filter(e -> !Objects.equals(e, element))
                .toList();
    }

    public static List<Integer> removeAllOccurrencesStreamFilterPredicate(List<Integer> list, int numToRemove) {
        return list.stream()
                .filter(new Predicate<Integer>() {
                    @Override
                    public boolean test(Integer integer) {
                        return integer != numToRemove;
                    }
                })
                .collect(Collectors.toUnmodifiableList());
    }

    public List<Integer> removeAllOccurencesNewList(List<Integer> list, int element) {
        List<Integer> remainingNumber = new ArrayList<>();
        for (Integer number : list) {
            if (!Objects.equals(element, number)) {
                remainingNumber.add(number);
            }
        }
        return remainingNumber;
    }

    public void removeAllOccurrencesRemoveIf(List<Integer> list, int element) {
        list.removeIf(e -> Objects.equals(e, element));
        System.out.println(list);
    }

    public void removeAllOccurrencesRemoveIfPredicate(List<Integer> list, int element) {
        list.removeIf(new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return integer == element;
            }
        });
        System.out.println(list);
    }

    public void removeAllOccurrencesIterator(List<Integer> list, int element) {
        for (Iterator<Integer> iterator = list.iterator(); iterator.hasNext();) {
            Integer number = iterator.next();
            if (Objects.equals(element, number)) {
                iterator.remove();
            }
        }
        System.out.println(list);
    }

    public void removeAllOccurrencesFor(List<Integer> list, Integer element) {
        for (int i = 0; i < list.size(); i++) {
            if (Objects.equals(element, list.get(i))) {
                list.remove(i);
                i--;
            }
        }
        System.out.println(list);
    }

    public void removeAllOccurrencesWhile(List<Integer> list, Integer element) {
        while (list.remove(element));
        System.out.println(list);
    }

    public void removeAllOccurrencesIndexOf(List<Integer> list, Integer element) {
        int index;
        while ((index = list.indexOf(element)) >= 0) {
            list.remove(element);
        }
        System.out.println(list);
    }

    public void removeAllOccurrencesContains(List<Integer> list, Integer element) {
        while (list.contains(element)) {
            list.remove(element);
        }
    }
}
