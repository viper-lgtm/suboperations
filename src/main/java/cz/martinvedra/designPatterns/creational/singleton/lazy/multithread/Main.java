package cz.martinvedra.designPatterns.creational.singleton.lazy.multithread;

public class Main {
    public static void main(String[] args) {
        Storage storageA = Storage.getStorageInstance();
        Storage storageB = Storage.getStorageInstance();
        storageA.addToList(1);
        storageB.addToList(2);
        System.out.println(storageA.printList());
    }
}
