package cz.martinvedra.Comparator;

import java.util.Comparator;

public class SortStudentByNumber implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.index - o2.index;
    }
}
