package cz.martinvedra.designPatterns.creational.builder.outsideClass;

public class ToyMain {
    public static void main(String[] args) {
        Toy toy = new ToyBuilder()
                .withName("Teddy")
                .withColor("brown")
                .build();
        System.out.println(toy);
    }
}
