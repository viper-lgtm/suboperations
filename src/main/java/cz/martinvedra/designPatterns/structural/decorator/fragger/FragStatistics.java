package cz.martinvedra.designPatterns.structural.decorator.fragger;

public interface FragStatistics {
    int incrementFragCount();
    int incrementDeathCount();
    void reset();
}
