package cz.martinvedra.designPatterns.behavioral.state;

public interface ParkingTicketVendingMachineState {
    void moveCreditCardToSensor();
    void pressPrintingButton();
    void openMachineAndAddPrintingPaperPieces();
}
