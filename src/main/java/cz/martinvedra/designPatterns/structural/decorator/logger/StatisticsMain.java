package cz.martinvedra.designPatterns.structural.decorator.logger;

import java.util.List;

public class StatisticsMain {
    public static void main(String[] args) {
        final StatisticsLogger statisticsLogger = new WithMeanStatisticsLoggerr(
                new WithSummaryStatisticsLogger(
                        new ExecutionTimesBaseStatistics(List.of(1.2, 2.2, 3.4))));
        statisticsLogger.displayStatistics();
    }
}
