package cz.martinvedra.designPatterns.structural.facade.seperate;

public class BCryptEncryptor implements Encryptor {
    @Override
    public String encrypt(final String toEncrypt) {
        return "encrypting " + toEncrypt + "with BCrypt";
    }
}
