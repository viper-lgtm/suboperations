package cz.martinvedra.designPatterns.behavioral.memento.editorText;

import java.util.ArrayDeque;
import java.util.Deque;

public class EditorTextManager {

    private Deque<EditorTextMemento> mementos = new ArrayDeque<>();

    public void save(EditorText editorText) {
        mementos.push(new EditorTextMemento(editorText));
    }

    public EditorTextMemento restore() {
        return mementos.pop();
    }
}
