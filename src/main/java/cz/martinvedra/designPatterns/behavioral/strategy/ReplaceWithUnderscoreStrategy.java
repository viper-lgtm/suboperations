package cz.martinvedra.designPatterns.behavioral.strategy;

public class ReplaceWithUnderscoreStrategy implements SpacesModificationStrategy {
    @Override
    public String modify(String input) {
        StringBuilder stringBuilder = new StringBuilder(input.length());
        for (char c : input.toCharArray()) {
            if (c != ' ') {
                stringBuilder.append(c);
            } else {
                stringBuilder.append('_');
            }
        }
        return stringBuilder.toString();
    }
}
