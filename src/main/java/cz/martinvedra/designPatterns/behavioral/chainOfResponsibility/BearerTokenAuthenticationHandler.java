package cz.martinvedra.designPatterns.behavioral.chainOfResponsibility;

import java.util.Random;

public class BearerTokenAuthenticationHandler implements AuthenticationHandler {
    @Override
    public boolean authenticate(Credentials credentials) {
        if (supports(credentials.getClass())) {
            return isTokenValid(credentials);
        }
        return false;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(BearerToken.class);
    }

    private boolean isTokenValid(Credentials credentials) {
        return (new Random().nextInt(3) % 3) != 0;
    }
}
