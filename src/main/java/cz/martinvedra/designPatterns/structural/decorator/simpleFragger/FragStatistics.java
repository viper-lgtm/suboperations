package cz.martinvedra.designPatterns.structural.decorator.simpleFragger;

public interface FragStatistics {
    int incrementFragCount();
    int incrementDeathCount();
    void reset();
}
