package cz.martinvedra.designPatterns.structural.decorator.simpleFragger;

public class Main {
    public static void main(String[] args) {
        FragStatistics statistics = new FirstPersonShooterFragStatistics();

        statistics.incrementDeathCount(); // nothing appears on the screen
        statistics.incrementFragCount(); // nothing appears on the screen

        FragStatistics decoratedStatistics = new DisplayCountersDecorator(new DeathCountInfoDecorator(statistics));
        decoratedStatistics.incrementFragCount();
        decoratedStatistics.incrementFragCount();
        decoratedStatistics.incrementFragCount();
        decoratedStatistics.incrementDeathCount();

    }
}
