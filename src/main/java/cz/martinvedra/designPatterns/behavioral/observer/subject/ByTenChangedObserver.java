package cz.martinvedra.designPatterns.behavioral.observer.subject;

public class ByTenChangedObserver extends Observer {
    private int previousState;
    public ByTenChangedObserver(Subject subject) {
        super(subject);
        this.previousState = subject.getState();
    }

    @Override
    public void update() {
        if (Math.abs(subject.getState() - previousState) >= 10) {
            System.out.println("Value changed by ten and is now: " + subject.getState());
        }
        previousState = subject.getState();
    }
}
