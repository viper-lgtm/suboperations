package cz.martinvedra.designPatterns.behavioral.memento.editorText;

public class EditorTextMain {
    public static void main(String[] args) {
        EditorText editorText = new EditorText();
        EditorTextManager editorTextManager = new EditorTextManager();
        editorText.addText("Ahoj");
        editorTextManager.save(editorText);
        editorText.addText("Hello");
        editorTextManager.save(editorText);
        System.out.println(editorText);

        editorTextManager.restore();
        EditorTextMemento restore = editorTextManager.restore();

        editorText.restoreFromMemento(restore);
        System.out.println(editorText);

    }
}
