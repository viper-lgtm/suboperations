package cz.martinvedra.designPatterns.behavioral.iterator.iterator;

public interface Car {
    String getVehicleInfo();
}
