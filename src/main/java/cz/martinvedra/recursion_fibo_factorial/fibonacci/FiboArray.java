package cz.martinvedra.recursion_fibo_factorial.fibonacci;

import java.util.Arrays;

public class FiboArray {
    public static void main(String[] args) {
        int n = 5;
        int[] f = new int[n + 2];
        f[0] = 0;
        f[1] = 1;

        for (int i = 2; i <= n; i++) {
            f[i] = f[i - 1] + f[i - 2];
        }

        Arrays.asList(f).forEach(System.out::println);
    }
}
