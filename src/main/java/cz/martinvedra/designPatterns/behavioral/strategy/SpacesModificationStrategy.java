package cz.martinvedra.designPatterns.behavioral.strategy;

public interface SpacesModificationStrategy {
    String modify(String input);
}
