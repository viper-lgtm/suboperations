package cz.martinvedra.designPatterns.structural.facade.allInOne;

public class OrderFacade {
    private DeliveryService deliveryService;
    private PaymentService paymentService;
    private ProductAvailabilityService productAvailabilityService;

    public OrderFacade(DeliveryService deliveryService, PaymentService paymentService, ProductAvailabilityService productAvailabilityService) {
        this.deliveryService = deliveryService;
        this.paymentService = paymentService;
        this.productAvailabilityService = productAvailabilityService;
    }

    public boolean placeOrder(Long prodcutId, int amount, String recipient) {
        if (productAvailabilityService.isAvailable(prodcutId)) {
            paymentService.pay(prodcutId, amount);
            deliveryService.deliverProduct(prodcutId, amount, recipient);
            return true;
        }
        return false;
    }
}
