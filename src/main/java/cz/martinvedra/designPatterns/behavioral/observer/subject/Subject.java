package cz.martinvedra.designPatterns.behavioral.observer.subject;

import java.util.ArrayList;
import java.util.List;

public class Subject {
    private List<Observer> observers = new ArrayList<>();
    private int state = 0;

    public Subject() {

    }

    public void subscribe(Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    public void changeStateBy(int change) {
        state += change;
        observers.forEach(Observer::update);
    }

    public int getState() {
        return state;
    }

}
