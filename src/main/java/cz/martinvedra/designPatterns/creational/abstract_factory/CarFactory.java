package cz.martinvedra.designPatterns.creational.abstract_factory;

public interface CarFactory {
    Car createSedan();
    Car createCombi();
    Car createHatchback();
}
