package cz.martinvedra.designPatterns.structural.facade.allInOne;

public interface DeliveryService {
    void deliverProduct(Long productsId, int amount, String recipient);
}
