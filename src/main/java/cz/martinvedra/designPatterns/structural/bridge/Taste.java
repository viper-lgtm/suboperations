package cz.martinvedra.designPatterns.structural.bridge;

public enum Taste {
    BITTER, SWEET, CHOCOLATE, NUT, LEMON
}
