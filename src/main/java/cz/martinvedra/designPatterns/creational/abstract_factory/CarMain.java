package cz.martinvedra.designPatterns.creational.abstract_factory;

public class CarMain {
    public static void main(String[] args) {
        CarType carType = CarType.AUDI_A4;
        CarFactory factory = new FactoryProvider().createFactory(carType);
        Car combi = factory.createCombi();
        System.out.println(combi);
    }
}
