package cz.martinvedra.designPatterns.creational.singleton.eager.class_based;

public class SimpleCounter {
    private static final SimpleCounter SIMPLE_COUNTER = new SimpleCounter();
    private int count = 0;
    private SimpleCounter() {}

    public static SimpleCounter getSimpleCounterInstance() {
        return SIMPLE_COUNTER;
    }

    public void incrementCount() {
        count++;
    }

    public int getCount() {
        return count;
    }

}
