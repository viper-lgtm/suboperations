package cz.martinvedra.nextFriday;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write a date in a format yyyy-MM-dd and you'll find out when is next Friday, yep");
        String stringDate = scanner.nextLine();
        LocalDate date = LocalDate.parse(stringDate);
        while (date.getDayOfWeek() != DayOfWeek.FRIDAY) {
            date = date.plusDays(1);
        }
        System.out.println(date);


    }
}
