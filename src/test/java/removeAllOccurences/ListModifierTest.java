package removeAllOccurences;

import cz.martinvedra.removeAllOccurences.ListModifier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ListModifierTest {

    private ListModifier listModifier;

    @BeforeEach
    public void setUp() {
        listModifier = new ListModifier();
    }

    @Test
    public void removeAllOccurencesTest() {
        //given
        List<Integer> integers = new ArrayList<>(List.of(1, 2, 3));
        int valueToRemove = 1;

        // when
        listModifier.removeAllOccurences(integers, valueToRemove);

        // then
        Assertions.assertEquals(List.of(2, 3), integers);
        assertThat(integers)
                .isEqualTo(List.of(2, 3));
    }

    @Test
    public void removeAllOccurencesWithNewListTest() {
        //given
        List<Integer> integers = new ArrayList<>(List.of(1, 2, 3));
        int valueToRemove = 1;

        // when
        List<Integer> integers1 = listModifier.removeAllOccurrencesStreamFilterNewList(integers, valueToRemove);

        // then
        Assertions.assertEquals(List.of(2, 3), integers1);
        assertThat(integers1)
                .isEqualTo(List.of(2, 3));
    }


}
