package cz.martinvedra.designPatterns.behavioral.chainOfResponsibility;

import java.util.UUID;

public class AwsSignature implements Credentials {
    @Override
    public String getCredentials(String userId) {
        return UUID.randomUUID().toString();
    }
}
