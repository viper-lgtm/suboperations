package cz.martinvedra.caesarCipher;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        info();
        while (true) {
            String cmd = scanner.nextLine();
            if ("exit".equals(cmd)) {
                break;
            }

            switch (cmd) {
                case "enc" -> encryptText();
                case "dec" -> decryptText();
                default -> System.out.println("Unknown cmd");
            }
        }
    }

    private static void decryptText() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert shift and press Enter");
        int shift = scanner.nextInt();
        System.out.println("Insert ciphered text in small letters and press enter");
        scanner.nextLine();
        char[] cipheredText = scanner.nextLine().toCharArray();
        char[] text = new char[cipheredText.length];
        for (int i = 0; i < cipheredText.length; i++) {
            if (cipheredText[i] == ' ') {
                text[i] = cipheredText[i];
            }

            int charCode = (cipheredText[i] - shift) - 96;
            charCode = charCode % 26 + 96;
            text[i] = (char) charCode;
        }
        System.out.println(text);
    }

    private static void encryptText() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert shift and press Enter");
        int shift = scanner.nextInt();
        System.out.println("Insert text in small letters and press enter");
        scanner.nextLine();
        char[] text = scanner.nextLine().toCharArray();
        char[] cipheredText = new char[text.length];
        for (int i = 0; i < text.length; i++) {
            if (text[i] == ' ') {
                cipheredText[i] = text[i];
            }

            int charCode = (text[i] + shift) - 96;
            charCode = charCode % 26 + 96;
            cipheredText[i] = (char) charCode;
        }
        System.out.println(cipheredText);
    }

    private static void info() {
        System.out.println("Sipher text with Caesar cipher");
        System.out.println("Write enc for encrypt or dec for decrypt and ENTER");
    }
}
