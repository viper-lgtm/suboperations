package cz.martinvedra.designPatterns.structural.adapter.pupil_adapter;

import java.util.ArrayList;
import java.util.List;

public class StudentMain {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();
        studentList.add(new PupilAdapter(new Pupil("John", "Doe", "johny@gmail.com", 20, List.of(1, 2, 3, 4))));
        for (Student student : studentList) {
            System.out.println(student.getFullName()); // Andrzej Nowak
            System.out.println(student.getContactDetails()); // anowak@sda.pl
            System.out.println(student.getResults()); // [3, 4, 5]
            System.out.println(student.isAdult()); // true
        }
    }
}
