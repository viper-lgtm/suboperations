package cz.martinvedra.designPatterns.creational.abstract_factory;

public interface Car {
    Type getType();
    String getModelName();
    Integer getCylindersNum();
    String getProducer();
    Float getEngineVolume();
    Integer getTrunkSize();
}
