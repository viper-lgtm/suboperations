package cz.martinvedra.designPatterns.structural.decorator.logger;

import java.util.List;

public interface StatisticsLogger {
    void displayStatistics();
    List<Double> getExecutionTimes();
}
