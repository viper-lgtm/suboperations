package cz.martinvedra.Comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(1, "Neo"));
        students.add(new Student(2, "Trinity"));
        students.add(new Student(3, "Morpheus"));

        for (Student student : students) {
            System.out.println(student);
        }

        Comparator<Student> sc = Collections.reverseOrder(new SortStudentByNumber());
        Collections.sort(students, sc);

        for (Student student : students) {
            System.out.println(student);
        }
    }
}
