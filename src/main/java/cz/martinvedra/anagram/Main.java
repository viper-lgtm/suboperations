package cz.martinvedra.anagram;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s1 = "McDonald's restaurants";
        String s2 = "Uncle Sam's standard rot";

        System.out.println("Write command:\n" +
                "1 = method using replaceFirst\n" +
                "2 = method using StringBuilder\n" +
                "3 = method using Char array and counter\n" +
                "4 = method using Arrays sort\n" +
                "5 = method using Map and Set"
        );

        while (true) {
            String s = scanner.nextLine();
            if ("exit".equals(s)) break;
            switch (s) {
                case "1" -> method1(s1, s2);
                case "2" -> method2(s1, s2);
                case "3" -> method3(s1, s2);
                case "4" -> method4(s1, s2);
                case "5" -> method5(s1, s2);
                default -> System.out.println("Wrong command");
            }
        }

    }

    private static void method5(String s1, String s2) {
        s1 = s1.replaceAll("\\s+", "").toLowerCase();
        s2 = s2.replaceAll("\\s+", "").toLowerCase();
        if (s1.length() != s2.length()) {
            System.out.println("Sorry, not anagrams, different length");
            return;
        }

        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s1.length(); i++) {
            if (map.containsKey(s1.charAt(i))) {
                map.put(s1.charAt(i), map.get(s1.charAt(i)) + 1);
            } else {
                map.put(s1.charAt(i), 1);
            }
        }

        for (int i = 0; i < s2.length(); i++) {
            if (map.containsKey(s2.charAt(i))) {
                map.put(s2.charAt(i), map.get(s2.charAt(i) - 1));
            } else {
                return;
            }
        }

        Set<Character> keySet = map.keySet();
        for (Character key : keySet) {
            if (map.get(key) != null) {
                System.out.println("Not anagram");
                return;
            }
        }

        System.out.println("Ok, apparently anagrams");

    }

    private static void method4(String s1, String s2) {
        char[] ch1 = s1.replaceAll("\\s+", "").toLowerCase().toCharArray();
        char[] ch2 = s2.replaceAll("\\s+", "").toLowerCase().toCharArray();
        if (ch1.length != ch2.length) {
            System.out.println("Not anagrams");
            return;
        }

        Arrays.sort(ch1);
        Arrays.sort(ch2);
        if (Arrays.equals(ch1, ch2)) {
            System.out.println("Anagrams spotted");
        } else {
            System.out.println("No anagrams today...");
        }
    }

    private static void method3(String s1, String s2) {
        char[] ch1 = s1.replaceAll("\\s+", "").toLowerCase().toCharArray();
        char[] ch2 = s2.replaceAll("\\s+", "").toLowerCase().toCharArray();
        if (ch1.length != ch2.length) {
            System.out.println("Not anagrams");
            return;
        }
        int ch1Counter = 0;
        int ch2Counter = 0;

        for (int i = 0; i < ch1.length; i++) {
            ch1Counter += ch1[i];
            ch2Counter += ch2[i];
        }

        if (ch1Counter == ch2Counter) {
            System.out.println("We have some anagrams here!");
        } else {
            System.out.println("Ops? No anagrams?");
        }
    }

    private static void method2(String s1, String s2) {
        s1 = s1.replaceAll("[\\s+'+]", "").toLowerCase();
        s2 = s2.replaceAll("[\\s+'+]", "").toLowerCase();

        if (s1 == "" || s2 == "") {
            System.out.println("Not anagrams, ops");
            return;
        }
        int iteration = s1.length();
        StringBuilder sb = new StringBuilder(s2);

        for (int i = 0; i < s1.length(); i++) {
            int index = sb.indexOf("" + s1.charAt(i));
//            int index = sb.indexOf(String.valueOf(str1.charAt(i)));
            if (index == -1) {
                System.out.println("Not anagrams");
                break;
            } else {
                sb.deleteCharAt(index);
                iteration--;
                if (sb.length() == 0 && iteration == 0) {
                    System.out.println("Words are anagrams, yep!");
                }
            }
        }
    }

    private static void method1(String s1, String s2) {
        s1 = s1.replaceAll("[\\s+'+]", "").toLowerCase();
        s2 = s2.replaceAll("[\\s+'+]", "").toLowerCase();

        if (s1 == "" || s2 == "") {
            System.out.println("Not anagrams, ops");
            return;
        }

        int iteration = s1.length();
        for (int i = 0; i < s1.length(); i++) {
//                s2 = s2.replaceFirst(Pattern.quote(String.valueOf(s1.charAt(i))), "");
            s2 = s2.replaceFirst(String.valueOf(s1.charAt(i)), "");
            iteration--;
            System.out.println("text: " + s2 + " with length: " + s2.length() + ", iteration: " + iteration);
            if (s2.length() > iteration) {
                System.out.println("Not anagrams!");
                break;
            }
            if (s2.equals("") && iteration == 0) {
                System.out.println("Hoooray, anagrams");
            }
        }
    }
}
