package cz.martinvedra.recursion_fibo_factorial.fibonacci;

public class FiboForLoop {
    public static void main(String[] args) {
        int end = 10;
        int n1 = 0;
        int n2 = 1;
        int n3;
        for (int i = 0; i < end; i++) {
            System.out.print(n1 + " ");
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
        }
    }
}
