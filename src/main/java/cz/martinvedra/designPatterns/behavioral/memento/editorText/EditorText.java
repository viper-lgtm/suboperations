package cz.martinvedra.designPatterns.behavioral.memento.editorText;

public class EditorText {

    private String value = "";

    public String getValue() {
        return value;
    }

    public void addText(String textToAdd) {
        value += textToAdd;
    }

    public void restoreFromMemento(EditorTextMemento memento) {
        value = memento.getValue();
    }

    @Override
    public String toString() {
        return "EditorText{" +
                "value='" + value + '\'' +
                '}';
    }
}
