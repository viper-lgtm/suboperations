package cz.martinvedra.designPatterns.structural.decorator.logger;

import java.util.List;

public class WithMeanStatisticsLoggerr implements StatisticsLogger {
    private final StatisticsLogger statisticsLogger;

    public WithMeanStatisticsLoggerr(StatisticsLogger statisticsLogger) {
        this.statisticsLogger = statisticsLogger;
    }

    @Override
    public void displayStatistics() {
        final double averageExTime = getExecutionTimes().stream().mapToDouble(x -> x).sum() / getExecutionTimes().size();
        System.out.println("Average execution time is " + averageExTime);
    }

    @Override
    public List<Double> getExecutionTimes() {
        return statisticsLogger.getExecutionTimes();
    }
}
