package cz.martinvedra.designPatterns.structural.bridge;

public interface DrinkPurchase {
    Drink buy(final Double cost);
}
