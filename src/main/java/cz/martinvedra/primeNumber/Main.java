package cz.martinvedra.primeNumber;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write a number greater then 1 and I tell you if it is prime");
        boolean isPrime = true;
        int n = scanner.nextInt();
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                isPrime = false;
            }
        }
        if (isPrime) {
            System.out.println("It is prime number, wow");
        } else {
            System.out.println("Not prime");
        }
    }
}
