package cz.martinvedra.designPatterns.behavioral.command;

public interface Command {
    void apply();
    void cancel();
}
