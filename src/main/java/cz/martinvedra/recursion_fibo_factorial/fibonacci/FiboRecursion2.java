package cz.martinvedra.recursion_fibo_factorial.fibonacci;

public class FiboRecursion2 {
    static int n1 = 0;
    static int n2 = 1;
    static int n3;

    public static void main(String[] args) {
        int end = 10;
        System.out.print(n1 + " " + n2);
        fibo(end - 2);
    }

    public static void fibo(int end) {
        if (end > 0) {
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            System.out.print(" " + n3);
            fibo(end - 1);
        }
    }
}
