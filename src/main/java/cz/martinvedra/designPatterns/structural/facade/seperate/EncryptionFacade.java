package cz.martinvedra.designPatterns.structural.facade.seperate;

public class EncryptionFacade implements Encryptors {
    private BCryptEncryptor bCryptEncryptor;
    private SCryptEncryptor sCryptEncryptor;
    private NoOpEncryptor noOpEncryptor;

    public EncryptionFacade(BCryptEncryptor bCryptEncryptor, SCryptEncryptor sCryptEncryptor, NoOpEncryptor noOpEncryptor) {
        this.bCryptEncryptor = bCryptEncryptor;
        this.sCryptEncryptor = sCryptEncryptor;
        this.noOpEncryptor = noOpEncryptor;
    }

    @Override
    public String encryptWithoutModification(String toEncrypt) {
        return noOpEncryptor.encrypt(toEncrypt);
    }

    @Override
    public String encryptWithBCrypt(String toEncrypt) {
        return bCryptEncryptor.encrypt(toEncrypt);
    }

    @Override
    public String encryptWithSCrypt(String toEncrypt) {
        return sCryptEncryptor.encrypt(toEncrypt);
    }
}
