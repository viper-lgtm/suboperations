package cz.martinvedra.designPatterns.creational.builder.task;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog.DogBuilder()
                .withName("Azor")
                .withType("longhair")
                .withAge(5)
                .withToys(List.of("Ball", "Stick"))
                .build();
        System.out.println(dog);
    }
}
