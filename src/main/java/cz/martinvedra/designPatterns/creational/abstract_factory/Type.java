package cz.martinvedra.designPatterns.creational.abstract_factory;

public enum Type {
    SEDAN,
    HATCHBACK,
    COMBI
}
