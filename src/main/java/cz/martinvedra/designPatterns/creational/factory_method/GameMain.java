package cz.martinvedra.designPatterns.creational.factory_method;

public class GameMain {
    public static void main(String[] args) {
        GameFactory gameFactory;
        String type = "PC";
        if (type.equals("PC")) {
            gameFactory = new ValorantGameCreator();
        } else if (type.equals("Board")) {
            gameFactory = new MonopolyGameCreator();
        } else {
            throw new RuntimeException("unknow game type");
        }
        System.out.println(gameFactory);
//        Game game = gameFactory.create();
//        System.out.println(game);
    }
}
