package cz.martinvedra.designPatterns.behavioral.command;

import java.util.List;
import java.util.stream.Collectors;

public class RemoveEmptyLinesCommand implements Command {
    private JavaFile javaFile;

    public RemoveEmptyLinesCommand(JavaFile javaFile) {
        this.javaFile = javaFile;
    }

    @Override
    public void apply() {
        List<String> nonEmptyLines = javaFile.getLinesContent().stream()
                .filter(line -> !line.trim().isEmpty())
                .collect(Collectors.toList());
        javaFile.setLinesContent(nonEmptyLines);
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Cancalling this operation is not possible");
    }
}
