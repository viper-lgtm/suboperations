package cz.martinvedra.recursion_fibo_factorial.fibonacci;

public class FiboWhile {
    public static void main(String[] args) {
        int n1 = 0;
        int n2 = 1;
        int n3;
        int end = 10;
        int count = 0;
        while (count < end) {
            System.out.print(n1 + " ");
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            count++;
        }
    }
}
