package cz.martinvedra.designPatterns.behavioral.interpreter;

public enum MathOperation {
    ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE,
    EXPONENTIATION
}
