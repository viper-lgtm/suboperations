package cz.martinvedra.recursion_fibo_factorial.fibonacci;

public class FiboRecursion3 {
    public static void main(String[] args) {
        int end = 10;
        for (int i = 0; i < end; i++) {
            System.out.print(fibo(i) + " ");
        }
    }

    public static int fibo(int n) {
        if (n <= 1) {
            return n;
        }
        return fibo(n - 1) + fibo(n - 2);
    }
}
