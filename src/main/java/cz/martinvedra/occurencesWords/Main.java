package cz.martinvedra.occurencesWords;

import java.util.Map;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        System.out.println("Count word occurence in the text");
        String text = "Mr. Cat and Mrs. Cat got a small kitty cat";
        String[] words = text.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");
        Map<String, Integer> wordsOccurences = new TreeMap<>();

        for (String word : words) {
            Integer count = wordsOccurences.get(word);
            if (count != null) {
                count++;
            } else {
                count = 1;
            }
            wordsOccurences.put(word, count);
        }
        wordsOccurences.forEach((key, val) -> System.out.println(key + " - " + val));
    }
}
