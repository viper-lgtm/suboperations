package cz.martinvedra.designPatterns.behavioral.chainOfResponsibility;

public class AwsAuthenticationHandler implements AuthenticationHandler {
    @Override
    public boolean authenticate(Credentials credentials) {
        if (supports(credentials.getClass())) {
            return authenticateInAws(credentials);
        }
        return false;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(AwsSignature.class);
    }

    private boolean authenticateInAws(Credentials credentials) {
        return credentials.getCredentials("someUserId").length() == 5;
    }
}
