package cz.martinvedra.recursion_fibo_factorial.factorial;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

//        System.out.println("FOR 1 - 5");
//        int n = scanner.nextInt();
//        int f = n;
//        for (int i = 1; i < n; i++) {
//            f *= i;
//        }
//        System.out.println(f);
//        System.out.println();

//        System.out.println("FOR 5 - 1");
//        int n = scanner.nextInt();
//        int f = n;
//        for (int i = n - 1; i >= 1 ; i--) {
//            f *= i;
//        }
//        System.out.println(f);

//        System.out.println("WHILE - Decrement in the condition");
//        int n = scanner.nextInt();;
//        int f = n;
//        while (--n > 0) {
//            f *= n;
//        }
//        System.out.println(f);
//        System.out.println();

//        System.out.println("WHILE classic");
//        int n = scanner.nextInt();;
//        int f = 1;
//        while (n > 0) {
//            f *= n;
//            n--;
//        }
//        System.out.println(f);
//        System.out.println();

//        System.out.println("Recursion");
//        System.out.println(getFactorialRecursion(5));
//        System.out.println();

//        String b = "5";
//        System.out.println("Recursion with BigInteger");
//        System.out.println(getFactorialRecursionBigInteger(new BigInteger(b)));
//        System.out.println();


    }

    private static int getFactorialRecursion(int num) {
        if (num == 1) {
            return 1;
        }
        return num * getFactorialRecursion(num - 1);
    }

    private static BigInteger getFactorialRecursionBigInteger(BigInteger num) {
        if(num.equals(BigInteger.ONE)) {
            return BigInteger.ONE;
        }
        return num.multiply(getFactorialRecursionBigInteger(num.subtract(BigInteger.ONE)));
    }


}
