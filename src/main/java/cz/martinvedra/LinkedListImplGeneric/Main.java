package cz.martinvedra.LinkedListImplGeneric;

public class Main {
    public static void main(String[] args) {
        GenericLinkedListImpl<String> stringList = new GenericLinkedListImpl<>();
        stringList.add("John");
        stringList.add("Max");
        stringList.add("Kate");
        System.out.println(stringList);
        System.out.println(stringList.getListLength());

        stringList.addAtPosition("Neo", 2);
        System.out.println(stringList);
        System.out.println(stringList.getListLength());

        stringList.removeByKey("Max");
        System.out.println(stringList);
        System.out.println(stringList.getListLength());

        System.out.println("Is list empty: " + stringList.isEmpty());
    }
}
