package cz.martinvedra.designPatterns.behavioral.memento.editorText;

public class EditorTextMemento {

    private String value;

    public EditorTextMemento(EditorText editorText) {
        this.value = editorText.getValue();
    }

    public String getValue() {
        return value;
    }
}
