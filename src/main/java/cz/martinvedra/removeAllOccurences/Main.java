package cz.martinvedra.removeAllOccurences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        ListModifier listModifier = new ListModifier();
        int numToRemove = 1;

        System.out.println("Remove all occurrences stream filter");
        List<Integer> integers = new ArrayList<>(List.of(1, 1, 2, 3));
        List<Integer> integers1 = listModifier.removeAllOccurrencesStreamFilterNewList(integers, numToRemove);
        System.out.println(integers1);
        System.out.println();

        System.out.println("Remove all occurrences stream filter predicate");
        List<Integer> integerss = new ArrayList<>(List.of(1, 1, 2, 3));
        List<Integer> integerss1 = listModifier.removeAllOccurrencesStreamFilterPredicate(integerss, numToRemove);
        System.out.println(integerss1);
        System.out.println();

        System.out.println("Remove all occurrences new list");
        List<Integer> integers2 = new ArrayList<>(List.of(1, 1, 2, 3));
        List<Integer> integers3 = listModifier.removeAllOccurencesNewList(integers2, numToRemove);
        System.out.println(integers3);
        System.out.println();

        System.out.println("Remove all occurrences removeIf");
        List<Integer> integers4 = new ArrayList<>(List.of(1, 1, 2, 3));
        listModifier.removeAllOccurrencesRemoveIf(integers4, numToRemove);
        System.out.println();

        System.out.println("Remove all occurrences removeIf Predicate");
        List<Integer> integers5 = new ArrayList<>(List.of(1, 1, 2, 3));
        listModifier.removeAllOccurrencesRemoveIfPredicate(integers5, numToRemove);
        System.out.println();

        System.out.println("Remove all occurrences Iterator");
        List<Integer> integers6 = new ArrayList<>(List.of(1, 1, 2, 3));
        listModifier.removeAllOccurrencesIterator(integers6, numToRemove);
        System.out.println();

        System.out.println("Remove all occurrences For");
        List<Integer> integers7 = new ArrayList<>(List.of(1, 1, 2, 3));
        listModifier.removeAllOccurrencesFor(integers7, numToRemove);
        System.out.println();

        System.out.println("Remove all occurrences While");
        List<Integer> integers8 = new ArrayList<>(List.of(1, 1, 2, 3));
        listModifier.removeAllOccurrencesWhile(integers8, numToRemove);
        System.out.println();

        System.out.println("Remove all occurrences indexOf");
        List<Integer> integers9 = new ArrayList<>(List.of(1, 1, 2, 3));
        listModifier.removeAllOccurrencesIndexOf(integers8, numToRemove);
        System.out.println();

        System.out.println("Remove all occurrences contains");
        List<Integer> integers10 = new ArrayList<>(Arrays.asList(1, 1, 2, 3));
        listModifier.removeAllOccurrencesContains(integers10, numToRemove);
        System.out.println(integers10);


    }
}
