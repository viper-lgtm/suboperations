package cz.martinvedra.designPatterns.behavioral.interpreter;

public class InterpreterMain {
    public static void main(String[] args) {
        MathOperationApplier mathOperationApplier = new MathOperationApplier();
        Interpreter interpreter = new PythonStyleWithoutOrderMathOperationsInterpreter(mathOperationApplier);

//        String result = interpreter.interpret(args[0]);
//        System.out.println(result);

        Interpreter interpreter1 = new WordsWithoutOrderMathOperationsInterpreter(mathOperationApplier);
        String result1 = interpreter1.interpret(args[1]);
        System.out.println(result1);
    }
}
